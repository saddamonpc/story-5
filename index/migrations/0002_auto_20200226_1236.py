# Generated by Django 3.0.3 on 2020-02-26 05:36

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('index', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Data',
            fields=[
                ('name', models.CharField(max_length=30, primary_key=True, serialize=False)),
                ('hobby', models.CharField(max_length=30)),
                ('fnb', models.CharField(max_length=30)),
            ],
        ),
        migrations.CreateModel(
            name='Year',
            fields=[
                ('year', models.IntegerField(primary_key=True, serialize=False)),
            ],
        ),
        migrations.DeleteModel(
            name='Friend',
        ),
        migrations.AddField(
            model_name='data',
            name='your_year',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='index.Year'),
        ),
    ]
