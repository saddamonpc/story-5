from django.shortcuts import render
from .forms import Data

from .models import Data, Year
from .forms import DataForm

# Create your views here.
def home(request):
    return render(request, 'index/index.html')

def profile(request):
    return render(request, 'index/profile.html')

def projects(request):
    return render(request, 'index/projects.html')

def friends(request):
    friends = Data.objects.all()
    return render(request, 'index/friends.html', {"friends":friends})

def add_friend(request):
    if request.method == 'POST':
        form = DataForm(request.POST)
        if form.is_valid():
            form.save()
            friends = Data.objects.all()
            return render(request, 'index/friends.html', {"friends":friends})
    
    else:
        form = DataForm()
    
    return render(request, 'index/friends.html')