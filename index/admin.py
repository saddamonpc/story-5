from django.contrib import admin
from .models import Data, Year

# Register your models here.
admin.site.register(Year)
admin.site.register(Data)